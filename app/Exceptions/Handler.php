<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use App\Traits\ApiResponser;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    use  ApiResponser;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
            Log::debug($e);
        });

        $this->renderable(function (Throwable $e, $request) {
            if ($request->expectsJson()) {
                return  $this->handleException($e, $request) ;
            }
        });
    }

    public function handleException(Throwable  $exception, $request)
    {
        if ($exception instanceof ValidationException) {
            $errors = $exception->validator->errors()->getMessages();
            return $this->errorResponse($errors, $exception->status);                                
        }

        if($exception instanceof NotFoundHttpException){            
            return $this->errorResponse('Recurso no encontrado' , 404);            
        }

        if($exception instanceof ModelNotFoundException){          
            $modelo=$exception->getModel() ; 
            return $this->errorResponse('No existe ninguna instancia de '.$modelo.' con el id especificado' , $exception->status);            
        }

        if ($exception instanceof AuthenticationException) {
            return $this->errorResponse("No autenticado", 401);
        }
        
        if ($exception instanceof AuthorizationException) {
            return $this->errorResponse('SIn autorización para cceder al recurso', 403);            
        }
        
        if ($exception instanceof QueryException) {
            return $this->errorResponse($exception->errorInfo[2], 409);
        } 

        if($exception instanceof MethodNotAllowedHttpException){
            return $this->errorResponse('metodo errado',405);            
        }

        if($exception instanceof HttpException){
            return $this->errorResponse($exception->getMessage(),$exception->getStatusCode());            
        }
        
        if(!config('app.debug')){
            return $this->errorResponse('falla inesperada',500); 
        }

       
        
        
        
       
    }
}
