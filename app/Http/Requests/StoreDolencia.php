<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDolencia extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return True;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nombre'=> 'required|unique:dolencias',
            'description'=> 'required',
        ];
    }

    public function attributes(){
        return [
            'nombre'=>'Nombre de dolencia',
            'description'=> 'Descripción de la dolencia',
        ];
    }

    public function messages(){
        return [
            'nombre.required'=>'Debe ingresar un nombre de la dolencia',
            'nombre.unique'=>'Nombre repetido',
            'description.required'=> 'Debe ingresar la descripción de la dolencia',
        ];
    }
}
