<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePlanta extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return True;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'nombre'=> 'required|unique:plantas',
            'descripcion'=> 'required',
            'imagen'=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',            
            ];
    }

    public function attributes(){
        return [
            'nombre'=>'Nombre de la planta',
            'descripcion'=> 'Descripción de la planta',
            'imagen'=> 'Imagen d ela planta',
        ];
    }

    public function messages(){
        return [
            'nombre.required'=>'Debe ingresar un nombre de la planta',            
            'descripcion.required'=> 'Debe ingresar la descripción de la planta',
            'imagen.required'=> 'Debe subir una imagen de la planta',
        ];
    }
}
