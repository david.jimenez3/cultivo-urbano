<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserCreated;
use Illuminate\Support\Facades\Auth;


class UserController extends ApiController
{
    const INDEX='users.index';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 
    public function index()
    {
        //        
        $users = User::all();
        return view('admin.usuarios.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $request->validate([
            'name'=> 'required',
            'email'=> 'required|email|unique:users',            
            'password'=> 'required|min:6|confirmed',            
        ]);
        
        $campos = $request->all();
        $campos['password']=Hash::make($request->input('password'));
        $campos['verificado']=User::USUARIO_NO_VERIFICADO;
        $campos['verificacion_token']=User::generarVerificacionToken();
        $campos['admin']=User::USUARIO_REGULAR;        
        $usuario =User::create($campos);               
        $token=$usuario->createToken('mismatas')->accessToken; 
        return $this->showOne($token,$usuario,200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $usuario=User::findOrFail($id);
        return $this->showOne("Usuario existente",$usuario,200); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
        return view('admin.usuarios.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,User $user)
    {
        //        
        $request->validate([
            'email'=> 'required|email',
            'name'=> 'required',            
        ]);
        
        $user->update($request->all());
        return redirect()->route(self::INDEX)->with('info', 'Dispositivo editado con exito');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verify($token,Request $request){
       $user=User::where('verificacion_token',$token)->firstOrFail();
        $user->verificado=User::USUARIO_VERIFICADO;
        $user->verificacion_token=null;
        $user->save();
        
        if ($request->expectsJson()) {            
            return $this->showOne($token,$user);            
        }else{
            return view('confirmarmail', compact('user'));            
        }
        
    }

    public function resend(User $user,Request $request){
        if($user->verificado==="1"){
            return $this->errorResponse("Usuario ya verificado",409);
        }
        
        retry(5, function () use ($user){
            Mail::to($user)->send(new UserCreated($user));
        } ,100);        

        if ($request->expectsJson()) {            
             return $this->showOne('Correo reenviado correctamente',$user);            
        }else{
             return view('confirmarmail', compact('user'));            
        }         
     }

    public function login(Request $request){
        
         
        $credenciales=$request->only('email','password');
        
        if (!Auth::attempt($credenciales)) {
            return $this->errorResponse("No auth",409);
        } elseif(Auth::user()->verificado==="1"){
            $token = Auth::user()->createToken('mismatas')->accessToken;
            $responseArray = [];
            $responseArray['token'] = $token;
            $responseArray['code'] = 200;
            $responseArray['name'] = Auth::user();            
            return response()->json($responseArray,200);
            # code...
        }else{
            return $this->errorResponse("Usuario no verificado",409);
        }
     }
}
