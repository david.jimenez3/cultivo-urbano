<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\Planta;

class ListaPLantasController extends ApiController
{
    //
    public function consultarPlantas(Request $request){
        
        $plantas=Planta::orderBy('nombre')->get();
        if ($request->expectsJson()) {
            return $this->showAll('Datos encontrados', $plantas);
        } 
    }

}
