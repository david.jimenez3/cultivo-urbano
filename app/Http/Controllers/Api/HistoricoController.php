<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\Historico;
use App\Models\Registroserial;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Parametro;

class HistoricoController extends ApiController
{
    //
    /**
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private $parametros;

    public function __construct()
    {
        $this->parametros=$this->construirArrayParametros();
    }

    private function construirArrayParametros()
    {
        $parametros_array=[];
        foreach (Parametro::all() as $parametro) {
            $parametros_array[$parametro->llave]=$parametro->valor;
        }
        return $parametros_array;
    }
    

    

    public function consultarDispositivoCultivoUrbano(Request $request)
    {
        $request->validate([
            'dispositivo_id'=>'required',
        ]);
        
        $consulta_dispositivo_nube=$this->consultarDispositivoNube(Auth::user()->id, $request->dispositivo_id);
        
        if ($consulta_dispositivo_nube['code']==200) {
            return $this->showOne($consulta_dispositivo_nube['mensaje'], $consulta_dispositivo_nube['datos_historico']);
        } else {
            return $this->errorResponse($consulta_dispositivo_nube['mensaje'], $consulta_dispositivo_nube['code']);
        }
    }

    private function consultarDispositivoNube($user_id, $dispositivo_id)
    {
        $id_dispositivo_nube=Registroserial::with('dispositivo', 'planta.limites')
        ->where('dispositivo_id', $dispositivo_id)
        ->where('user_id', $user_id)
        ->first();

        if (is_null($id_dispositivo_nube)) {
            return ['code'=>404,'mensaje'=>'Dispositivo no registrado en el sistema'];
        }
        $consulta_nube_iot = $this->consultarNube($id_dispositivo_nube->dispositivo);

        if ($consulta_nube_iot['status']==200) {
            $tipo=$this->calcularTipoMedicion($consulta_nube_iot, $id_dispositivo_nube->planta->limites);
            $consulta_nube_iot['planta_user_id']=$id_dispositivo_nube->id;
            $consulta_nube_iot['tipo']= $tipo['tipo'];            
            $historico=Historico::create(array_merge($consulta_nube_iot, $tipo['limite_excedido'], ['estado'=>'conectado']));
            $historico['fecha']=$historico->created_at->format('Y-m-d');
            $historico['hora']=$historico->created_at->format('H:i:s');
            $historico['dispositivo_id']=$id_dispositivo_nube->dispositivo_id;
            return ['code'=>200,'mensaje'=>'Historico registrado exitoso','datos_historico'=>$historico];
        } else {
            return ['code'=>$consulta_nube_iot['status'],'mensaje'=>$consulta_nube_iot['mensaje']];
        }
    }
    
    private function consultarNube($dispositivo)
    {
        $token_peticion=$this->crearTokenPeticion();
        if ($token_peticion->successful()) {
            $token_peticion=$token_peticion->collect()->all();
            return $this->consultarDatosDispositivo($token_peticion['token'], $dispositivo->serial);
        } else {
            $respuesta_error=implode("--", $token_peticion->collect()->all());
            return ['status'=>$token_peticion->status(),'mensaje'=> $respuesta_error];
        }
    }

    private function crearTokenPeticion()
    {
        $ubidots_key=$this->parametros['token_iot'];
        
        $ubidoits_url="{$this->parametros['endpoint_iot']}auth/token/";
        
        return Http::withHeaders(['x-ubidots-apikey' => $ubidots_key])->post($ubidoits_url);
    }

    private function consultarDatosDispositivo($token, $serial)
    {
        $url_dispositivo="{$this->parametros['endpoint_iot']}devices/{$serial}";
        
        $solicitud_dispositivo_iot=Http::withHeaders(['X-Auth-Token' => $token])->get($url_dispositivo);
        
        $dispositivo_iot=$solicitud_dispositivo_iot->collect();
        
        
        if ($solicitud_dispositivo_iot->failed()) {
            return ['status'=>401,'mensaje'=>'Dispositivo no encontrado' ];
        }

        $formato_fecha_ultima_actividad = Carbon::createFromTimestamp((int)substr((string)$dispositivo_iot['last_activity'], 0, -3))->setTimezone('America/Bogota');
        $hora_consulta=Carbon::now();

        $inactividad_dispositivo=$formato_fecha_ultima_actividad->diffInMinutes($hora_consulta);
        
        if ($inactividad_dispositivo < intval($this->parametros['tiempo_consulta_iot'])) {           
            
            return $this->consultarVariablesDispositivo($dispositivo_iot['variables_url'], $token);
        } else {
            return ['status'=>401,'mensaje'=>'Dispositivo sin conexion' ];
        }
    }

    private function consultarVariablesDispositivo($url, $token)
    {
        $solicitud_variables_iot=Http::withHeaders(['X-Auth-Token' => $token])->get($url);
        $variables=$solicitud_variables_iot->collect();
        $medicion=[];
        foreach ($variables['results'] as $variable) {
            $medicion[$variable['name']]=round($variable['last_value']['value'], 2);
        }
        $medicion['status']=200;
        
        return ($medicion);
    }

    private function calcularTipoMedicion($medicion, $limites)
    {
        $limite_excedido=[];
        foreach ($limites as $limite) {
            $caracteristica=strtolower($limite->caracteristica);
            if ($medicion[$caracteristica] <$limite->minimo) {
                $diferencia= $medicion[$caracteristica]-$limite->minimo;
            } elseif ($medicion[$caracteristica]>$limite->maximo) {
                $diferencia=$medicion[$caracteristica]-$limite->maximo;
            }
            if (isset($diferencia)) {
                $limite_excedido["desfase_{$caracteristica}"]= round($diferencia, 2);
            }
            unset($diferencia);
        }
        
        $tipo = empty($limite_excedido) ? 'normal':'alerta';
        
        return ['tipo'=>$tipo,'limite_excedido'=>$limite_excedido];
    }

    public function consultarHistorico(Request $request)
    {
        $user=Auth::user()->id;
        $request->validate([
            'dispositivo_id'=>'required',
        ]);
 
        $historico=$this->obtenerHistorico($request->dispositivo_id, $user);

        if ($historico->dispositivo->estado==0) {
            return $this->errorResponse('Dispositivo deshabilitado', 404);
        }
        
        if (count($historico->historicos)==0) {
            //if (true) {
            $historico_crear=$this->consultarDispositivoNube($user, $request->dispositivo_id);
            if ($historico_crear['code']==200) {
                $historico_obtener=$this->obtenerHistorico($request->dispositivo_id, $user);
                
                return $this->showOne($historico['mensaje'], $historico_obtener);
            } else {
                return $this->errorResponse('No hay historicos', 404);
            }
        } else {
            
            return $this->showOne('Historico obtenido', $historico);
        }
    }

    private function obtenerHistorico($dispositivo_id, $user_id)
    {
        $historico=Registroserial::with(['historicos' => function ($query) {
            $query->where('estado', 'conectado');
        }])->where(['dispositivo_id'=>$dispositivo_id,'user_id'=>$user_id])        
        ->first() ;
        return $historico;
        
    }
}
