<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\Registroserial;
use Illuminate\Support\Facades\Auth;

class ConsultarDispositivoController extends ApiController
{
    //
    public function consultarDispositivoRegistrado(){
        $dispositivo=Registroserial::where('user_id', Auth::user()->id)
        ->get();
        if (count($dispositivo)>0) {
            return $this->showAll('Datos encontrados', $dispositivo);
        }else{
            return $this->errorResponse('No tiene dispositivos registrados', []);
        }
    }
}
