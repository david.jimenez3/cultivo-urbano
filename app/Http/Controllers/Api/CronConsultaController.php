<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Log;
use App\Models\Parametro;
use App\Models\Dispositivo;
use App\Mail\AlertaIoT;
use App\Models\User;
use App\Models\Historico;

class CronConsultaController extends Controller
{
    //

    private $parametros;

    public function __construct()
    {
        $this->parametros=$this->construirArrayParametros();
    }

    private function construirArrayParametros()
    {
        $parametros_array=[];
        foreach (Parametro::all() as $parametro) {
            $parametros_array[$parametro->llave]=$parametro->valor;
        }
        return $parametros_array;
    }

    public function consultarEstadoDispositivos()
    {
        $token_peticion=$this->crearTokenPeticion();
        
        if ($token_peticion->successful()) {
            $token_peticion=$token_peticion->collect()->all();
            $this->consultarDispositivosActivos($token_peticion);
        } else {
            Log::channel('iot_cron')->error($token_peticion->collect()->all());
        }
    }

    private function crearTokenPeticion()
    {
        $ubidots_key=$this->parametros['token_iot'];
        
        $ubidoits_url="{$this->parametros['endpoint_iot']}auth/token/";
        
        return Http::withHeaders(['x-ubidots-apikey' => $ubidots_key])->post($ubidoits_url);
    }

    private function consultarDispositivosActivos($token_peticion)
    {
        $dispositivos=Dispositivo::with('registroseriales')
            ->where('estado', 1)
            ->get(); 
           
        foreach ($dispositivos as $dispositivo) {            
            if($dispositivo->registroseriales->count()){      
                $this->consultarVariableDispositivos($token_peticion, $dispositivo);
            }
        }
    }

    private function calcularTipoMedicion($medicion, $dispositivo)
    {
        

        $limites=$dispositivo->registroseriales->first()->planta->limites;
        
        $limite_excedido=[];
        foreach ($limites as $limite) {
            $caracteristica=strtolower($limite->caracteristica);
            if ($medicion[$caracteristica] <$limite->minimo) {
                $diferencia= $medicion[$caracteristica]-$limite->minimo;
            } elseif ($medicion[$caracteristica]>$limite->maximo) {
                $diferencia=$medicion[$caracteristica]-$limite->maximo;
            }

            if (isset($diferencia)) {
                $limite_excedido["desfase_{$caracteristica}"]= round($diferencia, 2);
            }
            unset($diferencia);
        }
        
        $alerta = empty($limite_excedido) ? false:true;
        Log::channel('iot_cron')->info($limite_excedido);
        if ($alerta) {
            $limite_excedido['tipo']='alerta';
            $limite_excedido['estado']='conectado';
            $limite_excedido['planta_user_id']=$dispositivo->registroseriales->first()->id;
            $this->registrarAlerta($dispositivo, array_merge($medicion, $limite_excedido));
        }
    }

    private function validarConexion($dispositivo)
    {
        $estado_conexion=[];
        if ($dispositivo->getStatusCode()==200) {
            $formato_fecha_ultima_actividad = Carbon::createFromTimestamp((int)substr((string)$dispositivo['last_activity'], 0, -3))->setTimezone('America/Bogota');
            $hora_consulta=Carbon::now();
            $inactividad_dispositivo=$formato_fecha_ultima_actividad->diffInMinutes($hora_consulta);
            
            $inactividad_dispositivo < $this->parametros['tiempo_consulta_iot'] ? $estado_conexion['estado']='conectado':$estado_conexion['estado']='desconectado';
            
            return $estado_conexion;
        } else {
            $estado_conexion['estado']='no_registrado';
            return $estado_conexion;
        }
    }

    private function consultarVariables($token_peticion, $variables_dispositivo_url)
    {
        $solicitud_variables=Http::withHeaders(['X-Auth-Token' => $token_peticion])->get($variables_dispositivo_url);
        
        if ($solicitud_variables->getStatusCode()==200) {
            $medicion=[];
            foreach ($solicitud_variables['results']  as $variable) {
                $medicion[$variable['name']]=round($variable['last_value']['value'], 2);
            }
            Log::channel('iot_cron')->info($medicion);
            return $medicion;
        } else {
            return false;
        }
    }

    private function consultarVariableDispositivos($token_peticion, $dispositivo)
    {
        $url_dispositivo="{$this->parametros['endpoint_iot']}devices/{$dispositivo->serial}";
        
        $solicitud_dispositivo_iot=Http::withHeaders(['X-Auth-Token' => $token_peticion])->get($url_dispositivo);
           
        $conexion=$this->validarConexion($solicitud_dispositivo_iot);
      
        if ($conexion['estado']=='conectado') {
            $vaiables_dispositivo = $this->consultarVariables($token_peticion, $solicitud_dispositivo_iot['variables_url']);
            
            if ($vaiables_dispositivo) {
                $this->calcularTipoMedicion($vaiables_dispositivo, $dispositivo);
            }
        } elseif ($conexion['estado']=='desconectado') {
            Log::channel('iot_cron')->info(['code'=>200,'mensaje'=>"Dispositivo desconectado {$dispositivo->serial}"]);
            $this->registrarAlerta($dispositivo);
        } else {
            Log::channel('iot_cron')->info(['code'=>$solicitud_dispositivo_iot->getStatusCode(),'mensaje'=>"Dispositivo no encontrado {$dispositivo->serial}"]);
        }
    }

    private function registrarAlerta($dispositivo, $dato_medicion=null)
    {
        $ultimo_registro_medicion=Historico::where('planta_user_id', $dispositivo->registroseriales->first()->id)->orderby('created_at', 'desc')->first();
        
        
        $planta_user_id=$dispositivo->registroseriales->first()->id;
        $user_id=$dispositivo->registroseriales->first()->user_id;
       

        if (is_null($ultimo_registro_medicion)) {
            $delta_ultimo_alerta= $this->parametros['tiempo_ultima_notificacion']+1;
        } else {
            $delta_ultimo_alerta=$ultimo_registro_medicion->created_at->diffInMinutes(Carbon::now());
        }
        $diferencia_timepo_notificar=$this->parametros['tiempo_ultima_notificacion']-$delta_ultimo_alerta;
        
        
        
        if($diferencia_timepo_notificar <= 0 ){
            
            if (is_null($dato_medicion)) {
                $dato_medicion=
                    [
                        "humedad" => 0,
                        "ph" => 0,
                        "temperatura" => 0,
                        "planta_user_id" =>$planta_user_id,
                        "tipo" => "alerta",
                        "estado" => "desconectado",
                        "planta_user_id"=>$dispositivo->registroseriales->first()->id
                ];

            }
            $historico=Historico::create($dato_medicion);
       
            $this->enviarAlerta($user_id, $historico);
        }
    }

    private function enviarAlerta($user_id, $historico)
    {
        $user=User::find($user_id);
        $formato_historico=[];
        $formato_historico['estado']=$historico->estado;
        $formato_historico['medidas']=[];
        array_push($formato_historico['medidas'], ['nombre'=>'Temperatura','valor'=>$historico->temperatura,'valor_desfase'=>$historico->desfase_temperatura]);
        array_push($formato_historico['medidas'], ['nombre'=>'Humedad','valor'=>$historico->humedad,'valor_desfase'=>$historico->desfase_humedad]);
        array_push($formato_historico['medidas'], ['nombre'=>'pH','valor'=>$historico->ph,'valor_desfase'=>$historico->desfase_ph]);
        
        $mensaje = $historico->estado == 'desconectado' ? 'Dispositivo desconectado' : 'Niveles de tu planta fuera de rango!';


        retry(5, function () use ($user, $formato_historico,$mensaje) {
            Mail::to($user->email)->send(new AlertaIoT($user, $formato_historico,$mensaje));
            Log::channel('iot_cron')->info(['respuesa_mail'=>Mail::failures()]);
        }, 100);
        
        $datos_notificacion = array(
            'app_id' => $this->parametros['onesignal_api_id'],
            'included_segments'=>'Subscribed Users',
            'data' => array("foo" => "bar"),
            'contents' => ["en"=> "{$mensaje}"]
        );
        
        $respuesta_onesignal=Http::withHeaders(
            ['Content-Type'=>'application/json',
            'Authorization' => "Basic {$this->parametros['onesignal_auth_token']}"]
        )->post($this->parametros['onesignal_endpoint'], $datos_notificacion);
        
        Log::channel('iot_cron')->info(['respuesa_onesignal'=>$respuesta_onesignal->collect()->all()]);
    }
}
