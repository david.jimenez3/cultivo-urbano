<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Models\Dispositivo;
use App\Models\Planta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Registroserial;
use Illuminate\Support\Facades\Auth;

class RegistroserialController extends ApiController
{
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Planta $planta)
    {
        //

        $request->validate([
            'serial'=> 'required',
            'alias'=>'required',
        ]);
      

        $serial=$request->serial;
        $dispositivo=Dispositivo::where('serial', $serial)->first();
        
        


        if ($dispositivo) {
            $user =Auth::user()->id;

            $busca_registro_duplicado=Registroserial::where('dispositivo_id', $dispositivo->id)->first();
            if (!is_null($busca_registro_duplicado)) {
                return $this->errorResponse('Ya registrado', 409);
            } else {
                return DB::transaction(function () use ($planta, $dispositivo, $user, $request) {
                    $registro = Registroserial::create(
                        [
                    'alias'=>$request->alias,
                    'planta_id'=>$planta->id,
                    'dispositivo_id'=>$dispositivo->id,
                    'user_id'=>$user
                ]
                    );
                    return $this->showOne('Dispositivo registrado', $registro);
                });
            };
        } else {
            return $this->errorResponse('Serial invalido', 409);
        }
    }

    

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
