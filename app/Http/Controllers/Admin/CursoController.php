<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\Planta;
use App\Models\Curso;

class CursoController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    const INDEX = 'curso.index'; 
    public function index(Request $request)
    {
        //
        $planta = Planta::with('cursos')->findOrFail($request->planta);
        
            if ($request->expectsJson()) {                
                return $this->showOneMany('Datos encontrados', $planta);
            } else {                
                return view('admin.curso.index', compact('planta'));
            }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $planta = Planta::with('cursos')->find($request->input('planta'));
        return view('admin.curso.create',compact('planta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Curso $curso)
    {
        //
        $request->validate([
            'titulo'=> 'required',
            'descripcion'=> 'required',  
            
        ]);

        $curso_nuevo=Curso::create($request->all());
        return redirect()->route(self::INDEX,['planta'=>$curso_nuevo->planta_id])->with('info','Curso agregdo con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,Curso $curso)
    {
        //
        if ($request->expectsJson()) {
            $curso=Curso::with('planta')->findOrFail($curso->id); 
            return $this->showOne('Dato encontrado',$curso);            
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Curso $curso)
    {
        //
        $planta=Planta::find($curso->planta_id);
        
        return view('admin.curso.edit',compact('curso','planta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Curso $curso)
    {
        //
        $request->validate([
            'titulo'=> 'required',
            'descripcion'=> 'required',  
            
        ]);
        $curso->update($request->all());
        return redirect()->route(self::INDEX,['planta'=>$curso->planta_id])->with('info','Curso editado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Curso $curso)
    {
        //
        $planta_id=$curso->planta_id;
        $curso->delete();
        return redirect()->route(self::INDEX,['planta'=>$planta_id])->with('info','Curso borrado con exito');
    }
}
