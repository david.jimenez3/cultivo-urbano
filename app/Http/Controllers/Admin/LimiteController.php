<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\Planta;
use App\Models\Limite;
use App\Rules\ValidRangeLimit;

class LimiteController extends ApiController
{
    const RANGO_VALIDACION='between:0,99.99';
    const INDEX='limites.index';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $planta = Planta::with('limites')->find($request->input('planta'));
        return view('admin.limite.index',compact('planta'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        
        $planta=Planta::with('dolencia')->find($request->input('planta'));
        return view('admin.limite.create',compact('planta'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Limite $limite)
    {
        //
        $request->validate([
            'caracteristica'=> 'required',
            'maximo'=> ['required',self::RANGO_VALIDACION,'numeric'],  
            'minimo'=>['required',self::RANGO_VALIDACION,'numeric']
        ]);
        
        $limites=Limite::create($request->all());

        return redirect()->route(self::INDEX,['planta'=>$limites->planta_id])->with('info','Limite agregdo con exito');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Limite $limite)
    {
        //
        
        $planta=Planta::find($limite->planta_id);
        
        return view('admin.limite.edit',compact('limite','planta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Limite $limite)
    {
        //
        $request->validate([
            'caracteristica'=> 'required',
            'maximo'=> ['required',self::RANGO_VALIDACION,'numeric'],  
            'minimo'=>['required',self::RANGO_VALIDACION,'numeric']
        ]);
        $limite->update($request->all());
        return redirect()->route(self::INDEX,['planta'=>$limite->planta_id])->with('info','Limite editado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Limite $limite)
    {
        //        
        $limite->delete();
        return redirect()->route(self::INDEX,['planta'=>$limite->planta_id])->with('info','Limite borrado con exito');
    }
}
