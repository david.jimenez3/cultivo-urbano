<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Parametro;

class ConfiguracionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    const INDEX = 'parametros.index'; 

    public function index()
    {
        //
        $parametros=Parametro::orderBy('llave','desc')->get();
        return view ('admin.parametros.index',compact('parametros'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view ('admin.parametros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'llave'=> 'required',
            'valor'=> 'required', 
        ]);
        Parametro::create($request->all());
        return redirect()->route(self::INDEX)->with('info', 'Parametro creada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Parametro $parametro)
    {
        //

        return view ('admin.parametros.edit',compact('parametro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Parametro $parametro)
    {
        //
        $request->validate([
            'llave'=> 'required',
            'valor'=> 'required', 
        ]);
        $parametro->update($request->all());
        return redirect()->route(self::INDEX)->with('info', 'Parametro editado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Parametro $parametro)
    {
        //
        
        $parametro->delete();
       
        return redirect()->route(self::INDEX)->with('info', 'Parametro eliminado con exito');
    }
}
