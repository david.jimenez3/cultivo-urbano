<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\Dolencia;
use App\Http\Requests\StoreDolencia;

class DolenciaController extends ApiController
{
    const INDEX='dolencias.index';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->expectsJson()) {
            $dolencia=Dolencia::orderBy('nombre')->get();
            return $this->showAll('Datos encontrados', $dolencia);
        } else {
            $dolencias = Dolencia::orderBy('nombre')->get();
            return view('admin.dolencia.index', compact('dolencias'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.dolencia.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDolencia $request)
    {
        Dolencia::create($request->all());
        return redirect()->route(self::INDEX)->with('info', 'Dolencia creada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Dolencia $dolencia)
    {
        //
        $dolencia=Dolencia::findOrFail($dolencia->id);
        return $this->showOne('Datos encontrados', $dolencia);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Dolencia $dolencia)
    {
        //
        return view('admin.dolencia.edit', compact('dolencia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dolencia $dolencia)
    {
        //
        $request->validate([
            'nombre'=> 'required',
            'description'=> 'required',
        ]);
        
        $dolencia->update($request->all());
        return redirect()->route(self::INDEX)->with('info', 'Dolencia editada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dolencia $dolencia)
    {
        //
        $dolencia->delete();
        return redirect()->route(self::INDEX)->with('info', 'Dolencia eliminada con exito');
    }
}
