<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Models\Dispositivo;
use App\Models\Registroserial;
use Illuminate\Support\Facades\Crypt;

class DispositivoController extends ApiController
{
    const INDEX='dispositivos.index';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dispositivos=Dispositivo::orderBy('serial')->get();

        
        return view('admin.dispositivo.index', compact('dispositivos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.dispositivo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'serial'=> 'required|unique:dispositivos',
            'estado'=> 'required',
            
        ]);
        $dispositivo=$request->all();
        $serial_encriptado= Crypt::encryptString($request->serial);
        $dispositivo['serial']=$serial_encriptado;
        Dispositivo::create($request->all());
        return redirect()->route(self::INDEX)->with('info', 'Dispositivo registrado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $dispositivos = Dispositivo::with('registroseriales')->whereHas('registroseriales', function ($q) use($id) {
            $q->where('user_id', $id);
        })->get();
        return view('admin.dispositivo.index', compact('dispositivos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Dispositivo $dispositivo)
    {
        return view('admin.dispositivo.edit', compact('dispositivo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dispositivo $dispositivo)
    {
        //
        $request->validate([
            'serial'=> 'required|unique:dispositivos',
        ]);
        $dispositivo->update($request->all());
        return redirect()->route(self::INDEX)->with('info', 'Dispositivo editado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dispositivo $dispositivo)
    {
        //
        $dispositivo->delete();
        return redirect()->route(self::INDEX)->with('info', 'Dispositivo eliminado con exito');
    }
}
