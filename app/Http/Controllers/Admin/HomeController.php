<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;

class HomeController extends ApiController
{
    //
    public function index(){
        
        return view('admin.dolencia.index');
    }
}
