<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Dolencia;
use App\Models\Planta;
use App\Http\Requests\StorePlanta;

class PlantaController extends ApiController
{


    const INDEX='plantas.index';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dolencia = Dolencia::with('plantas')->findOrFail($request->dolencia);
         
        if ($request->expectsJson()) {
            return $this->showOneMany('Datos encontrados', $dolencia);
        } else {
            return view('admin.planta.index', compact('dolencia'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $dolencia_id=$request->input('dolencia');
        return view('admin.planta.create', compact('dolencia_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePlanta $request)
    {
        //
        $atributos=$request->all();        
        Planta::create($atributos);
        return redirect()->route(self::INDEX, ['dolencia'=>$request->input('dolencia_id')])->with('info', 'Planta creada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Planta $planta)
    {
        //
        if ($request->expectsJson()) {
            $planta=Planta::with('dolencia')->findOrFail($planta->id);
            return $this->showOne('Dato encontrado', $planta);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Planta $planta)
    {
        return view('admin.planta.edit', compact('planta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Planta $planta)
    {
        $request->validate([
            'nombre'=> 'required',
            'descripcion'=> 'required',            
        ]);
        
        $atributos=$request->all();
       
        
        $planta->update($atributos);
        return redirect()->route(self::INDEX, ['dolencia'=>$planta->dolencia_id])->with('info', 'Planta editada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Planta $planta)
    {
        $dolencia_id=$planta->dolencia_id;
        Storage::delete($planta->imagen);
        $planta->delete();
        
        return redirect()->route(self::INDEX, ['dolencia'=>$dolencia_id])->with('info', 'Planta eliminada con exito');
    }
}
