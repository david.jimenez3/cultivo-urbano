<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Curso;
use App\Models\Leccion;

class LeccionController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    const INDEX='lecciones.index';
    
    public function index(Request $request)
    {
        //
        $curso = Curso::with('planta', 'lecciones')->findOrFail($request->curso);
        
        if ($request->expectsJson()) {
            return $this->showOneMany('Datos encontrados', $curso);
        } else {
            return view('admin.lecciones.index', compact('curso'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $curso = Curso::with('planta', 'lecciones')->find($request->input('curso'));
        return view('admin.lecciones.create', compact('curso'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Leccion $leccion)
    {
        //
        
        $request->validate([
            'titulo'=> 'required|unique:lecciones',
            'descripcion'=> 'required',        
        ]);
        $atributos=$request->all();        
        $lecciones =Leccion::create($atributos);
        return redirect()->route(self::INDEX, ['curso'=>$lecciones->curso_id])->with('info', 'Lección agregdo con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Leccion $leccione)
    {
        //
        if ($request->expectsJson()) {
            $leccion=Leccion::with('Curso')->findOrFail($leccione->id);
            return $this->showOne('Dato encontrado', $leccion);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Leccion $leccione)
    {
        //
        $curso = Curso::with('planta')->find($leccione->curso_id);
        return view('admin.lecciones.edit', compact('leccione', 'curso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Leccion $leccione)
    {
        //
        $request->validate([
            'titulo'=> 'required',
            'descripcion'=> 'required',
            'imagen'=>'mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $atributos=$request->all();       
        $leccione->update($atributos);

        return redirect()->route(self::INDEX, ['curso'=>$leccione->curso_id])->with('info', 'Lección editada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Leccion $leccione)
    {
        //
        $curso_id=$leccione;
        $leccione->delete();
        return redirect()->route(self::INDEX, ['curso'=>$curso_id])->with('info', 'Lección borrada con exito');
    }
}
