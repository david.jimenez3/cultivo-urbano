<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SignatureMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $header ='X-name')
    {
        //agreragr cabecera a la respuesta 1. respuesta luego add cabecera
        //aftermiddleware
        $response = $next($request); //la respuesta
        $response->headers->set($header,config('app.name'));
        return $response;
    }
}
