<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Transformers\DolenciaTransformer;

class Dolencia extends Model
{
    use HasFactory,SoftDeletes;

    protected $dates=['delete_at'];
    protected $fillable =[
            'nombre','description'
    ];
    protected $hidden = ['created_at','updated_at','deleted_at'];    
    public $transformer = DolenciaTransformer::class;
    public function plantas(){
        return $this->hasMany(Planta::class);
    }
}
