<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
Use App\Transformers\LeccionTransformer;
class Leccion extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'lecciones' ;
    protected $dates=['delete_at'];

    protected $hidden = ['created_at','updated_at','deleted_at'];

    public $transformer = LeccionTransformer::class;

    protected $fillable =[
        'curso_id','titulo','descripcion'
    ];
    
    public function curso()
    {
        return $this->belongsTo(Curso::class);
    }

    public function getImagenAttribute($imagen){
        return asset($imagen);
    }
}
