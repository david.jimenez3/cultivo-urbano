<?php

namespace App\Models;

use App\Transformers\UserTransformer;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Passport\HasApiTokens;
Use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    const USUARIO_NO_VERIFICADO='0';
    const USUARIO_VERIFICADO='1';
    const USUARIO_ADMINISTRADOR='true';
    const USUARIO_REGULAR='false';

    public $transformer = UserTransformer::class;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'verificado',
        'verificacion_token',
        'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
        'created_at',
        'updated_at',
        'deleted_at',
        'profile_photo_url',
        'profile_photo_path',
        'current_team_id',
        'email_verified_at',        
        'verificacion_token',
        'verificado',
        'admin'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function setNameAttribute($valor){
        $this->attributes['name']= strtolower($valor); 
    }

    public function setEmailAttribute($valor){
        $this->attributes['email']= strtolower($valor); 
    }

    public function esVerificado(){
        return $this->verified==User::USUARIO_VERIFICADO;
    }

    public function esAdministrador(){
        return $this->verified==User::USUARIO_ADMINISTRADOR;
    }

    public static function generarVerificacionTOken(){
        return  Str::random(40);    
    }

    public function registroseriales()
    {
        return $this->hasMany(Registroserial::class);
    }
    
}
