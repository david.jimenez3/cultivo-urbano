<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Transformers\CursoTransformer;

class Curso extends Model
{
    use HasFactory,SoftDeletes;

    protected $dates=['delete_at'];
    protected $fillable =[
        'planta_id','titulo','descripcion'
    ];

    protected $hidden = ['created_at','updated_at','deleted_at'];

    public $transformer = CursoTransformer::class;

    public function planta()
    {
        return $this->belongsTo(Planta::class);
    }

    public function lecciones()
    {
        return $this->hasMany(Leccion::class);
    }
}
