<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Limite extends Model
{
    use HasFactory,SoftDeletes;
    protected $dates=['delete_at'];
    protected $fillable =[
        'planta_id','caracteristica','maximo','minimo'
    ];

    public function planta()
    {
        return $this->belongsTo(Planta::class);
    }
}
