<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dispositivo extends Model
{
    use HasFactory,SoftDeletes;
    protected $dates=['delete_at'];
    protected $fillable =[
        'nombre','serial','estado'
    ];

    public function setSerialAttribute($valor){
        $this->attributes['serial']= strtoupper($valor); 
    }
    public function registroseriales()
    {
        return $this->hasMany(Registroserial::class);
    }
}
