<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Transformers\HistoricoTransformer;

class Historico extends Model
{
    use HasFactory,SoftDeletes;
    
    protected $fillable =[
        'planta_user_id','temperatura','ph','humedad','tipo','desfase_temperatura','desfase_humedad','desfase_ph','estado'
    ];
    protected $hidden = ['updated_at','deleted_at'];
    
    public $transformer = HistoricoTransformer::class;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    
    public function registroserial()
    {
        $this->belongsTo(Registroserial::class, 'planta_user_id');
    }
}
