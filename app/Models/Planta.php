<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Transformers\PlantaTransformer;

class Planta extends Model
{
    use HasFactory,SoftDeletes;
    protected $dates=['delete_at'];
    protected $fillable =[
        'dolencia_id','nombre','descripcion'
    ];

    protected $hidden = ['created_at','updated_at','deleted_at'];

    public $transformer = PlantaTransformer::class;

    public function getImagenAttribute($imagen){
        return asset($imagen);
    }

    public function dolencia()
    {
        return $this->belongsTo(Dolencia::class);
    }

    public function cursos()
    {
        return $this->hasMany(Curso::class);
    }

    public function limites()
    {
        return $this->hasMany(Limite::class);
    }

    public function registroseriales()
    {
        return $this->hasMany(Registroserial::class);
    }
}
