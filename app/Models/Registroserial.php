<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Transformers\PlantaUserTransformer;

class Registroserial extends Model
{
    use HasFactory;

    protected $table = 'planta_user' ;

    public $transformer = PlantaUserTransformer::class;

    public $timestamps = false;
    
    protected $fillable =[
        'alias','planta_id','dispositivo_id','user_id'
    ];

    public function planta()
    {
        return $this->belongsTo(Planta::class);
    }

    public function usuario()
    {
        return $this->belongsTo(User::class);
    }

    public function dispositivo()
    {
        return $this->belongsTo(Dispositivo::class);
    }

    public function historicos()
    {
        return $this->hasMany(Historico::class, 'planta_user_id');
    }
}
