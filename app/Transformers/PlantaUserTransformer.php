<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Registroserial;

class PlantaUserTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Registroserial $registro)
    {
        return [
            //
            
            'identificador'=>(int)$registro->id,
            'nombre'=>$registro->alias,
            'planta'=>(int)$registro->planta_id,
            'dispositivo'=>(int)$registro->dispositivo_id,
            'usuario'=>(int)$registro->user_id
        ];
    }
}
