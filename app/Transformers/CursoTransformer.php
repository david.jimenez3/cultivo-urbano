<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Curso;

class CursoTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Curso $curso)
    {
        return [
            //
            'identificador'=>(int)$curso->id,
            'planta'=>(int)$curso->planta_id,
            'curso'=>(string)$curso->nombre,
            'contenido'=>(string)$curso->descripcion,            
        ];
    }
}
