<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Planta;


class PlantaTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Planta $planta)
    {
        return [
            //
            'identificador'=>(int)$planta->id,
            'dolencia'=>(int)$planta->dolencia_id,
            'planta'=>(string)$planta->nombre,            
            'contenido'=>(string)$planta->descripcion,
            'foto'=>(string)$planta->imagen,
        ];
    }
}
