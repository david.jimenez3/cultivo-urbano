<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Historico;

class HistoricoTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Historico $historico)
    {
        return [
            //
            'identificador'=>(int)$historico->id,
            'valortemperatura'=>(float)$historico->temperatura,
            'valorph'=>(float)$historico->ph,
            'valorhumedad'=>(float)$historico->humedad,
            'medicion'=>(string)$historico->tipo,
            'registrodispositivo'=>(int)$historico->planta_user_id,
        ];
    }
}
