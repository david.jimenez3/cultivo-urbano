<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Dolencia;

class DolenciaTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Dolencia $dolencia)
    {
        return [
            //
            'identificador'=>(int)$dolencia->id,
            'dolencia'=>(string)$dolencia->nombre,
            'contenido'=>(string)$dolencia->description,            
        ];
    }
}
