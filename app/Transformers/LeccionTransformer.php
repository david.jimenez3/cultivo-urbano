<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Leccion;

class LeccionTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Leccion $leccion)
    {
        return [
            //
            'identificador'=>(int)$leccion->id,
            'curso'=>(int)$leccion->curso_id,
            'leccion'=>(string)$leccion->nombre,
            'contenido'=>(string)$leccion->descripcion,
            'portada'=>(string)$leccion->imagen,
        ];
    }
}
