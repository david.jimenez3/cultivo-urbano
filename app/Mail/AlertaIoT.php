<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Models\Historico;

class AlertaIoT extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $historicos;
    public $mensaje;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,Array $historico,String $mensaje)
    {
        //

        $this->user=$user;
        $this->historicos=$historico;
        $this->mensaje=$mensaje;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {          
        
        return $this->view('emails.alerta')->subject($this->mensaje);
    }
   
}
