<?php

namespace App\Providers;

use App\Mail\UserCreated;
use Illuminate\Support\ServiceProvider;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        User::created(function ($user){
            retry(5, function () use ($user){
                Mail::to($user)->send(new UserCreated($user));
            } ,100);
        });
        
    }
}
