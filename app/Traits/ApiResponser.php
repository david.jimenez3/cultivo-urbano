<?php
namespace App\Traits;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

trait ApiResponser{

	private function successResponse($message,$data,$code){
		
		return response()->json(['status'=>True,'message'=>$message,'code'=>$code,'data'=>$data]);
	}

	protected function errorResponse($message,$code){			
		return response()->json(['status'=>false,'message'=> $message,'code'=>$code,'data'=>[]]);	
	}
    
	protected function showAll($menssage,Collection $collection,$code=200){	
		
		$transformer = $collection->first()->transformer;
		
		if($collection->isEmpty()){
			return $this->successResponse($menssage,$collection,$code);	
		}
        $collection = $this->transformData($collection, $transformer);
		
		$collection =$this->cacheResponse($collection);
		
		return $this->successResponse($menssage,$collection,$code);	
	}

	protected function showOne($menssage,Model $instance,$code=200){	
		return $this->successResponse($menssage,$instance,$code);	
	}

	protected function showOneMany($menssage,Model $instance,$code=200){		
		
		return $this->successResponse($menssage,$instance,$code);	
	}

	protected function transformData($data,$transformer){
		
		$transformer =fractal($data,new $transformer);
		
		return $transformer->toArray();
	}

	protected function cacheResponse($data){
		$url = request()->url();
		return Cache::remember($url, 15/60, function () use($data) {
			return $data;
		});
	}

	

}
