<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call([
            UserSeeder::class,
            DolenciaSeeder::class,
            PlantaSeeder::class,
            CursoSeeder::class,
            LeccionSeeder::class,
            LimiteSeeder::class,
            DispositivoSeeder::class,
            RegistroDispositivoSeeder::class,
            ParametroSeeder::class
        ]);
    }
}

