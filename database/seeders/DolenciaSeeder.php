<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Dolencia;
use App\Models\Planta;

class DolenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Dolencia::flushEventListeners();
        Dolencia::create([
            'nombre'=>'Dolor de cabeza',
            'description'=>'Se produce en ocaciones por estres',
        ]);
        Dolencia::factory(10)->create();

        
    }
}
