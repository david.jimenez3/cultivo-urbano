<?php

namespace Database\Seeders;
use App\Models\Planta;


use Illuminate\Database\Seeder;

class PlantaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Planta::flushEventListeners();
        Planta::create([
            'nombre'=>'MArihuana',
            'descripcion'=>'PAra que NO sirve la marihuana',           
            'dolencia_id' => 1
        ]);
        Planta::factory(10)->create();
    }
}
