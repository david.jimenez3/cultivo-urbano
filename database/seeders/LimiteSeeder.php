<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Limite;

class LimiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Limite::flushEventListeners();
        Limite::create([
            'caracteristica'=>'Temperatura',
            'maximo'=>'27.20',
            'minimo'=>'9.20',
            'planta_id' => 1
        ]);
        Limite::create([
            'caracteristica'=>'Humedad',
            'maximo'=>'100',
            'minimo'=>'45',
            'planta_id' => 1
        ]);
        Limite::create([
            'caracteristica'=>'Ph',
            'maximo'=>'12',
            'minimo'=>'4',
            'planta_id' => 1
        ]);
    }
}
