<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Curso;

class CursoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Curso::flushEventListeners();
        Curso::create([
            'planta_id'=>1,
            'titulo'=>'Preparación',
            'descripcion'=>'En este curso aprenderas el proceso de preparación y germinación d euna semilla de marihuana...',            
        ]);
    }
}
