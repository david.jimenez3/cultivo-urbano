<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Leccion;

class LeccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Leccion::flushEventListeners();
        Leccion::create([
            'curso_id'=>1,
            'titulo'=>'Insumos',
            'descripcion'=>'Antes de comenzar debe ',             
        ]);
    }
}
