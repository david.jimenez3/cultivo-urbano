<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::flushEventListeners();        
        $user= new User();
        $user->name ='Administrador';
        $user->email = 'administrador@cultivo-urbano.com';
        $user->password = Hash::make('administrador');
        $user->admin='true';
        $user->verificado = User::USUARIO_VERIFICADO;        
        $user->verificacion_token='CKpemfokKoc2E3RsxiofBIeepDYI14Lx7g8C904E';        
        $user->save();
        $user= new User();
        $user->name ='David J';
        $user->email = 'david81ster@gmail.com';
        $user->password = Hash::make('123456');
        $user->admin='false';
        $user->verificado = User::USUARIO_VERIFICADO;        
        $user->verificacion_token='1KpemfokKoc2E3RsxiofBIeepDYI14Lx7g8C904E';        
        $user->save();
        User::factory(50)->create();
    }
}
