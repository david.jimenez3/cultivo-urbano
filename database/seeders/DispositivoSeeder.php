<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Dispositivo;

class DispositivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Dispositivo::flushEventListeners();
        Dispositivo::create([
            'serial'=>'C45BBE54D8F2',
            'estado'=>true,
        ]);
        Dispositivo::create([
            'serial'=>'C45BBE54D8F1',
            'estado'=>true,
        ]);
    }
}
