<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Parametro;

class ParametroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Parametro::flushEventListeners();
        Parametro::create([
            'llave'=>'token_iot',            
            'valor'=>'BBFF-b6527519466eec2c6de5d2b16346d781964',                    
        ]);
        Parametro::create([
            'llave'=>'endpoint_iot',            
            'valor'=>'https://industrial.api.ubidots.com/api/v1.6/',            
        ]);        
        Parametro::create([
            'llave'=>'tiempo_consulta_iot',
            'valor'=>'1440',
        ]);
        Parametro::create([
            'llave'=>'onesignal_api_id',            
            'valor'=>'45853b7c-e0f8-4131-9d71-328740d18fa9',            
        ]);
        Parametro::create([
            'llave'=>'onesignal_auth_token',            
            'valor'=>'MWI3MjNhMjktYWQwZS00YmM0LTllYWItN2QyYzhhZjNhMWRm',            
        ]);
        Parametro::create([
            'llave'=>'tiempo_ultima_notificacion',            
            'valor'=>'30',            
        ]);
        Parametro::create([
            'llave'=>'onesignal_endpoint',            
            'valor'=>'https://onesignal.com/api/v1/notifications',            
        ]);
        
        
    }
}
