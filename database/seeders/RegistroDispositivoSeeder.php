<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Registroserial;

class RegistroDispositivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Registroserial::flushEventListeners();
        Registroserial::create([
            'alias'=>'Mi Dispositivo',
            'planta_id'=>1,
            'user_id'=>1,
            'dispositivo_id'=>1
        ]);
        Registroserial::create([
            'alias'=>'Mi Otro Dispositivo',
            'planta_id'=>1,
            'user_id'=>1,
            'dispositivo_id'=>2
        ]);
    }
}
