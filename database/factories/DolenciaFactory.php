<?php

namespace Database\Factories;

use App\Models\Dolencia;
use Illuminate\Database\Eloquent\Factories\Factory;

class DolenciaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Dolencia::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'nombre' => "Dolencia-".$this->faker->word(),
            'description' => $this->faker->paragraph(),            
        ];
    }
}
