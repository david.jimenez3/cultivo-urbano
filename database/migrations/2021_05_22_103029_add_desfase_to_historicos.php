<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDesfaseToHistoricos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historicos', function (Blueprint $table) {
            //
            $table->float('desfase_temperatura')->nullable();         
            $table->float('desfase_ph')->nullable();         
            $table->float('desfase_humedad')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historicos', function (Blueprint $table) {
            //
            $table->dropColumn('desfase_temperatura');
            $table->dropColumn('desfase_ph');
            $table->dropColumn('desfase_humedad');
        });
    }
}
