<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecciones', function (Blueprint $table) {
            $table->id();  
            $table->foreignId('curso_id')
            ->constrained('cursos')
            ->onUpdate('cascade')
            ->onDelete('cascade');             
            $table->string('titulo');
            $table->longText('descripcion');
            $table->timestamps();       
            $table->softDeletes();             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecciones');
    }
}
