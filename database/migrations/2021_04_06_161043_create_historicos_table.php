<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historicos', function (Blueprint $table) {
            $table->id();            
            $table->foreignId('planta_user_id')
            ->constrained('planta_user')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->float('temperatura');         
            $table->float('ph');         
            $table->float('humedad');         
            $table->string('tipo'); 
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historicos');
    }
}
