<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlantasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plantas', function (Blueprint $table) {
            $table->id();            
            $table->foreignId('dolencia_id')
                    ->constrained('dolencias')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->string('nombre');            
            $table->longText('descripcion');
            $table->timestamps();    
            $table->softDeletes();        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plantas');
    }
}
