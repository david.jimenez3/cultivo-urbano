<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PlantaUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('planta_user', function (Blueprint $table) {        
            $table->id();
            $table->string('alias');
            $table->foreignId('planta_id')->constrained('plantas')->onUpdate('cascade')->onDelete('cascade'); 
            $table->foreignId('user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade'); 
            $table->foreignId('dispositivo_id')->unique()->constrained('dispositivos')->onUpdate('cascade')->onDelete('cascade'); 
            $table->unique(["planta_id", "dispositivo_id","user_id"], 'planta_user_unique');
        });      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('planta_user');
    }
}
