<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Admin\DolenciaController;
use App\Http\Controllers\Admin\PlantaController;
use App\Http\Controllers\Admin\CursoController;
use App\Http\Controllers\Admin\LeccionController;
use App\Http\Controllers\Api\RegistroserialController;
use App\Http\Controllers\Api\HistoricoController;
use App\Http\Controllers\Api\ConsultarDispositivoController;
use App\Http\Controllers\Api\ListaPLantasController;
use App\Http\Controllers\Api\CronConsultaController;

Route::resource('users', UserController::class)->only([ 'store']);
Route::get('users/verify/{token}', [UserController::class,'verify'])->name('verify');
Route::get('users/{user}/resend', [UserController::class,'resend'])->name('resend');
Route::post('login', [UserController::class,'login'])->name('apilogin');

//TODO eliminar al terminar el desarrollo del cron
Route::get('cron', [CronConsultaController::class,'consultarEstadoDispositivos']);


Route::middleware('auth:api')->group(function () {
    Route::resource('users', UserController::class)->only(['show']);
    Route::resource('dolencias', DolenciaController::class)->only(['index', 'show']);
    Route::resource('plantas', PlantaController::class)->only(['index', 'show']);
    Route::resource('cursos', CursoController::class)->only(['index', 'show']);
    Route::resource('lecciones', LeccionController::class)->only(['index', 'show']);
    Route::resource('plantas.registrardispositivos', RegistroserialController::class)->only(['store']);
    Route::post('consultardispositivo', [HistoricoController::class,'consultarDispositivoCultivoUrbano']);
    Route::get('consultarhistorico', [HistoricoController::class,'consultarHistorico']);
    Route::get('consultardispositivoregistrado', [ConsultarDispositivoController::class,'consultarDispositivoRegistrado']);
    Route::get('consultar-planta', [ListaPLantasController::class,'consultarPlantas']);
});
