<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\DolenciaController;
use App\Http\Controllers\Admin\PlantaController;
use App\Http\Controllers\Admin\DispositivoController;
use App\Http\Controllers\Admin\LimiteController;
use App\Http\Controllers\Admin\CursoController;
use App\Http\Controllers\Admin\LeccionController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Admin\ConfiguracionController;


Route::get('/', [DolenciaController::class, 'index']);
Route::resource('dolencias', DolenciaController::class)->except(['show']);
Route::resource('plantas', PlantaController::class)->except(['show']);
Route::resource('dispositivos', DispositivoController::class);
Route::resource('limites', LimiteController::class);
Route::resource('cursos', CursoController::class);
Route::resource('lecciones', LeccionController::class);
Route::resource('users', UserController::class);
Route::resource('parametros', ConfiguracionController::class);
