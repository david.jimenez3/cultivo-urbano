@extends('adminlte::page')

@section('title', 'Cultivo Urbano IoT')

@section('content_header')

@stop

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('dispositivos.index')}}">Dispositivos</a></li>
      <li class="breadcrumb-item active" aria-current="page">Crear</li>
    </ol>
  </nav>

    <div class="container">
        <div class="card">
            <div class="card-body">
                {!!Form::open(['route'=>'dispositivos.store'])!!}
                <div class="form-group">
                    {!! Form::label('name', 'Serial') !!}
                    {!! Form::text('serial', null, ['class' => 'form-control']) !!}
                    @error('serial')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="form-group">
                    {!! Form::label('name', 'Estado') !!}
                    {!! Form::select('estado', ['0' => 'Inactivo', '1' => 'Activo'], '1',['class' => 'form-select','aria-label'=>"Select Estado"]) !!}                    
                    @error('estado')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                {!! Form::submit('Guardar', ['class'=>'btn btn-primary']) !!}
                {!!Form::close([])!!}
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop