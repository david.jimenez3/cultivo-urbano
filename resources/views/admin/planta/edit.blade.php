@extends('adminlte::page')

@section('title', 'Cultivo Urbano IoT')

@section('content_header')
<h1>Editar Planta </h1>
@stop

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('dolencias.index')}}">Dolencias</a></li>
        <li class="breadcrumb-item"><a href="{{route('plantas.index', ['dolencia'=>$planta->dolencia_id])}}">Plantas</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Editar</li>
    </ol>
</nav>
<div class="container">
    <div class="card">
        <div class="card-body">
            {!!Form::model($planta,['route'=>['plantas.update',$planta],'method'=>'put', 'files' => true])!!}
            <div class="form-group">
                {!! Form::label('name', 'Nombre Planta') !!}
                {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
                @error('nombre')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="form-group">
                {!! Form::label('name', 'Descripción') !!}
                {!! Form::textarea('descripcion', null, ['class' => 'form-control','id'=>'summernote']) !!}
                @error('description')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>
            {!! Form::submit('Editar', ['class'=>'btn btn-primary']) !!}
            {!!Form::close([])!!}
        </div>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script type="text/javascript">
    $('#summernote').summernote({
        height: 400
    });
</script>
@stop