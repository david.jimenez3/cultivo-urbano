@extends('adminlte::page')

@section('title', 'Cultivo Urbano IoT')

@section('content_header')
    
@stop

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('parametros.index')}}">parametros</a></li>
      <li class="breadcrumb-item active" aria-current="page">Editar</li>
    </ol>
  </nav>

    <div class="container">
        <div class="card">
            <div class="card-body">
                {!!Form::model($parametro,['route'=>['parametros.update',$parametro],'method'=>'put'])!!}
                <div class="form-group">
                    {!! Form::label('name', 'Nombre parametro') !!}
                    {!! Form::text('llave', null, ['class' => 'form-control']) !!}
                    @error('llave')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="form-group">
                    {!! Form::label('name', 'Valor parametro') !!}
                    {!! Form::text('valor', null, ['class' => 'form-control']) !!}
                    @error('valor')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                {!! Form::submit('Editar', ['class'=>'btn btn-primary']) !!}
                {!!Form::close([])!!}
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop