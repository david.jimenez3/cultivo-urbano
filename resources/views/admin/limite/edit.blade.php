@extends('adminlte::page')

@section('title', 'Cultivo Urbano IoT')

@section('content_header')

@stop

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('dolencias.index')}}">Dolencias</a></li>
      <li class="breadcrumb-item"><a href="{{route('plantas.index', ['dolencia'=>$planta->dolencia_id])}}">Plantas</a></li>
      <li class="breadcrumb-item"><a href="{{route('limites.index', ['planta'=>$limite->planta_id])}}">Limites</a></li>
      <li class="breadcrumb-item active" aria-current="page">Editar</li>
    </ol>
  </nav>

    <div class="container">
        <div class="card">
            <div class="card-body">
                {!!Form::model($limite,['route'=>['limites.update',$limite],'method'=>'put'])!!}                
                <div class="form-group">
                    {!! Form::label('name', 'Caracteristica') !!}
                    {!! Form::text('caracteristica', null, ['class' => 'form-control']) !!}
                    @error('caracteristica')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="form-group">
                    {!! Form::label('name', 'Valor Máximo') !!}
                    {!! Form::text('maximo', null, ['class' => 'form-control']) !!}
                    @error('maximo')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="form-group">
                    {!! Form::label('name', 'Valor minimo') !!}
                    {!! Form::text('minimo', null, ['class' => 'form-control']) !!}
                    @error('minimo')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                
                {!! Form::submit('Guardar', ['class'=>'btn btn-primary']) !!}
                {!!Form::close([])!!}
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop