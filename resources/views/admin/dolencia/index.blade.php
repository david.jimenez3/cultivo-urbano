@extends('adminlte::page')

@section('title', 'Cultivo Urbano IoT')

@section('content_header')

@stop

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('dolencias.index')}}">Dolencias</a></li>      
    </ol>
  </nav>

                   <!-- Begin Page Content -->
                   <div class="container-fluid">

                    <!-- Page Heading -->
                    
                    @if(session('info'))            
                    <div class="row" id="alert_box">
                        <div class="col-12">
                            <div class="alert alert-success" role="alert">
                                <strong>{{session('info')}}</strong>
                            </div>
                        </div>
                    </div>
                    @endif
                    

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">                            
                            <a  class="btn btn-primary" role="button" href="{{route('dolencias.create')}}">Crear</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" aria-describedby="Lista de registros de la sección"  >
                                    <thead>
                                        <tr>
                                            <th scope="col">Dolencia</th>                                            
                                            <th scope="col">Acciones</th>    
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th scope="col">Dolencia</th>                                            
                                            <th scope="col">Acciones</th>                                            
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($dolencias as $dolencia)
                                        <tr>
                                            <td>{{$dolencia->nombre}}</td>
                                                                                        
                                            <td>
                                                <div class="acciones">
                                                    <a href="{{route('dolencias.edit',$dolencia)}}" class="tooltipped" data-position="bottom" data-toggle="tooltip" data-placement="top" title="Editar " >
                                                    <i class="fa fa-cog" aria-hidden="true"></i></a>
                                                    <a href ="{{route('plantas.index',['dolencia'=>$dolencia->id])}}" class="tooltipped" data-position="bottom" data-toggle="tooltip" data-placement="top" title="Ver plantas " ><i class="fas fa-leaf" aria-hidden="true"></i></a>
                                                    <a class="tooltipped" data-position="bottom"data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="showConfirmDeleteModal('{{$dolencia->nombre}}', '{{route('dolencias.destroy',$dolencia)}}')" >
                                                    <i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </div>
                                            </td>
                                        </tr>  
                                        @endforeach                                                                          
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

               
                    <!-- Modal -->
                    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Eliminar</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                Esta seguro de eliminar el registro <strong><span id="deleteName"></span>. 
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <form method="POST" id="deleteForm" accept-charset="UTF-8" style="display:inline-block">
                                @csrf
                                @method('delete')                    
                                <button type="submit" class="btn btn-danger" value="Confirmar">Confirmar                    
                             </form>
                            </div>
                        </div>
                        </div>
                    </div>


@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    } ); 
    
    @if(session('info'))
    $( "#alert_box" ).fadeOut( 5000, function() {

    });

    @endif

    function showConfirmDeleteModal(name, url) 
        {
            var modal= $('#modal1').modal();       
            console.log(name)
            console.log(url)
            $('#deleteForm').prop('action', url);
            $('#deleteName').text(name);
            modal.modal('show');  
        }
    
    </script>
@stop