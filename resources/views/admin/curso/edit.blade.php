@extends('adminlte::page')

@section('title', 'Cultivo Urbano IoT')

@section('content_header')

@stop

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('dolencias.index')}}">Dolencias</a></li>
      <li class="breadcrumb-item"><a href="{{route('plantas.index', ['dolencia'=>$planta->dolencia_id])}}">Plantas</a></li>
      <li class="breadcrumb-item"><a href="{{route('cursos.index', ['planta'=>$curso->planta_id])}}">Curso</a></li>
      <li class="breadcrumb-item active" aria-current="page">Editar</li>
    </ol>
  </nav>

    <div class="container">
        <div class="card">
            <div class="card-body">
                {!!Form::model($curso,['route'=>['cursos.update',$curso],'method'=>'put'])!!}                
                <div class="form-group">
                    {!! Form::label('name', 'Caracteristica') !!}
                    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
                    @error('titulo')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="form-group">
                    {!! Form::label('name', 'Descripción') !!}
                    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
                    @error('description')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                
                {!! Form::submit('Guardar', ['class'=>'btn btn-primary']) !!}
                {!!Form::close([])!!}
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop