@extends('adminlte::page')

@section('title', 'Cultivo Urbano IoT')

@section('content_header')

@stop

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('dolencias.index')}}">Dolencias</a></li>
        <li class="breadcrumb-item"><a
                href="{{route('plantas.index', ['dolencia'=>$curso->planta->dolencia_id])}}">Plantas</a></li>
        <li class="breadcrumb-item"><a href="{{route('cursos.index', ['planta'=>$curso->planta->id])}}">Cursos</a></li>
        <li class="breadcrumb-item"><a href="{{route('lecciones.index', ['curso'=>$curso->id])}}">Lecciones</a></li>
        <li class="breadcrumb-item active" aria-current="page">Editar</li>
    </ol>
</nav>

<div class="container">
    <div class="card">
        <div class="card-body">
            {!!Form::model($leccione,['route'=>['lecciones.update',$leccione],'method'=>'put'])!!}
            <div class="form-group">
                {!! Form::label('name', 'Titulo') !!}
                {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
                @error('titulo')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="form-group">
                {!! Form::label('name', 'Descripción') !!}
                {!! Form::textarea('descripcion', null, ['class' => 'form-control','id'=>'summernote']) !!}
                @error('description')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>

            {!! Form::submit('Guardar', ['class'=>'btn btn-primary']) !!}
            {!!Form::close([])!!}
        </div>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script type="text/javascript">
    $('#summernote').summernote({
        height: 400
    });
</script>
@stop