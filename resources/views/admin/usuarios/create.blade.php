@extends('adminlte::page')

@section('title', 'Cultivo Urbano IoT')

@section('content_header')

@stop

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('dolencias.index')}}">Dolencias</a></li>
      <li class="breadcrumb-item active" aria-current="page">Crear</li>
    </ol>
  </nav>

    <div class="container">
        <div class="card">
            <div class="card-body">
                {!!Form::open(['route'=>'dolencias.store'])!!}
                <div class="form-group">
                    {!! Form::label('name', 'Nombre Dolencia') !!}
                    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
                    @error('nombre')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="form-group">
                    {!! Form::label('name', 'Descripción') !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    @error('description')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                {!! Form::submit('Guardar', ['class'=>'btn btn-primary']) !!}
                {!!Form::close([])!!}
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop