@extends('adminlte::page')

@section('title', 'Cultivo Urbano IoT')

@section('content_header')
    
@stop

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('users.index')}}">Usuarios</a></li>
      <li class="breadcrumb-item active" aria-current="page">Editar</li>
    </ol>
  </nav>

    <div class="container">
        <div class="card">
            <div class="card-body">
                {!!Form::model($user,['route'=>['users.update',$user],'method'=>'put'])!!}
                <div class="form-group">
                    {!! Form::label('name', 'Nombre') !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    @error('nombre')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="form-group">
                    {!! Form::label('name', 'Email') !!}
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    @error('email')
                        <span class="text-danger">{{$message}}</span>
                    @enderror
                </div>
                
                {!! Form::submit('Editar', ['class'=>'btn btn-primary']) !!}
                {!!Form::close([])!!}
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop